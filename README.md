# Cumplo API
 
_Desafío generado por cumplo para postulación
El desafio consta del desarrollo de una API que entregue el TMC (Tasa de Interés Máxima Convencional) que corresponda a las condiciones de crédito_

## Como usarla
La ruta de la api es http://localhost:3000/api/v1/tmc
La api acepta tres parametros los cuales son:
- **amount**: monto en UF
- **days**: dias de plazo
- **date**: fecha a consultar el tmc

```
  Ejemplo: http://localhost:3000/api/v1/tmc?amount=1&days=90&date=1/1/2020
```

## Requisitos y configuración
### Ruby versión
    2.3.1 o superior

### Rails versión
    5.1.0 o superior

### DataBase 
    MySQL 5.7.16 o superior

### Configuración
    **Opción 1**
      - mysql -u root -p
      - ingresar Password
      - mysql> CREATE DATABASE cumplo CHARACTER SET utf8 COLLATE utf8_general_ci;
      - mysql> CREATE USER 'cumplo'@'localhost' identified by 'cumplo';
      - mysql> GRANT ALL PRIVILEGES ON cumplo.* TO cumplo@localhost;
      - mysql> FLUSH PRIVILEGES;


    **Opción 2**
      Modificar los datos de conexion a una base de datos ya creada en config/database.yml 

    **Ejecutar > rake db:migrate**

### Ejecutar pruebas
    rake test

### Deployment instructions
    rails s