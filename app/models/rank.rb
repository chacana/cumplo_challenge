class Rank < ApplicationRecord

  def self.find_by_mount_and_days(amount, days)
    Rank.where("initial_amount < ? AND final_amount >= ? ",amount, amount)
    .where("initial_days <= ? AND final_days > ? ", days, days)
  end

end
