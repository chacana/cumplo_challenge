module TmcUtils

  def get_tmc_by_code_rank(tmcs, ranks)
    tmcs_j = JSON.parse(tmcs.body)
    tmcs_j['TMCs'].each do |tmc|
      ranks.each do |rank|
        if tmc['Tipo'] == rank.code
          return tmc
        end
      end
    end
    return {}
  end
end