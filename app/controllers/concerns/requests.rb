module Requests

  def get_sbif_tmc(date_s)
    date = Date.strptime(date_s, "%d/%m/%Y")
    url = 'https://api.sbif.cl/api-sbifv3/recursos_api'
    endpoint = 'tmc'
    month = date.month
    year = date.year
    key = '9c84db4d447c80c74961a72245371245cb7ac15f'
    formato = 'json'

    final_url = "#{url}/#{endpoint}/#{year}/#{month}?apikey=#{key}&formato=#{formato}"
    begin
      return {status: 'Ok', data: RestClient.get(final_url)}
    rescue => exception 
      return {status: 'Error', errors: [{code: 'D01', descriptions: 'Error al conectar con Sbif o la informacion no esta disponible'}]}
    end
  end

end
