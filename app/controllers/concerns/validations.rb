module Validations
 

  def validate_input_data(params)
    errors = []
    if (['amount','days','date'] - params.keys).empty?

      params.each do |key, value|
        if key == 'amount'
          errors.concat(validate_amount(value))
        end

        if key == 'days'
          errors.concat(validate_days(value))
        end

        if key == 'date'
          errors.concat(validate_date(value))
        end
      end
    else
      errors << {code: 'E01', descriptions: 'Faltan paramertos de entrada'}
    end

    status = errors.compact.any? ? 'Error' : 'Ok'
    return {status: status, errors: errors.compact}
  end

  def validate_amount(amount)
    errors = []
    if amount.to_f <= 0
      errors << {code: 'A01', descriptions: 'El monto no puede ser negativo o cero '}
    end
    return errors
  end

  def validate_days(days)
    errors = []
    if days.to_i <= 0
      errors << {code: 'B01', descriptions: 'El plazo no puede ser negativo o cero '}
    end
    return errors
  end

  def validate_date(date)
    errors = []
    begin
      Date.strptime(date,"%d/%m/%Y")
    rescue => exception
      errors << {code: 'C01', descriptions: 'Formato de fecha invalido Ej: 23/11/2020'}
    end
    return errors
  end

end
