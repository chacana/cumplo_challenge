class Api::V1::TmcController < ApplicationController
  include Validations
  include Requests
  include TmcUtils

  def index

    resp_validation = validate_input_data(params)
    if resp_validation[:status] == 'Ok'
      resp_sbif_tmcs = get_sbif_tmc(params['date'])
      if resp_sbif_tmcs[:status] == 'Ok'
        ranks = Rank.find_by_mount_and_days(params['amount'], params['days'])
        if ranks.any?
          tmc = get_tmc_by_code_rank(resp_sbif_tmcs[:data], ranks)
          render json: {status: 'Ok', data: tmc}
        else
          render json: {status: 'Error', errors: {code: 'E01', descriptions: 'No se encontro datos para el rango de busqueda'}}
        end
      else
        render json: resp_sbif_tmcs
      end
    else
      render json: resp_validation
    end
  end
end
