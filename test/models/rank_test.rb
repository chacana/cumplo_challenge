require 'test_helper'

class RankTest < ActiveSupport::TestCase
   test "search rank" do
     rank = Rank.find_by_mount_and_days(1234, 35)
     assert_not_nil(rank, 'Rank found')
   end
   
   test "search rank with invalid parameters" do
    rank = Rank.find_by_mount_and_days(0, 0)
    assert_empty(rank, 'Rank not found')
  end
end
