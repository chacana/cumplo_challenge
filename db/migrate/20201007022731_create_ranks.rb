class CreateRanks < ActiveRecord::Migration[6.0]
  def up
    create_table :ranks do |t|
      t.string :description
      t.integer :initial_amount
      t.integer :final_amount
      t.integer :initial_days
      t.integer :final_days
      t.string :code
      t.timestamps
    end

    max_final_ammount = 10000000
    max_final_days = 36500

    Rank.create(
      description: 'Operaciones no reajustables en moneda nacional de menos de 90 días Superiores al equivalente de 5.000 unidades de fomento',
      initial_amount: 5000,
      final_amount: max_final_ammount,
      initial_days: 1,
      final_days: 90,
      code: '25');

    Rank.create(
      description: 'Operaciones no reajustables en moneda nacional de menos de 90 días Inferiores o iguales al equivalente de 5.000 unidades de fomento',
      initial_amount: 0,
      final_amount: 5000,
      initial_days: 1,
      final_days: 90,
      code: '26');



    Rank.create(
      description: 'Operaciones no reajustables en moneda nacional 90 días o más - Inferiores o iguales al equivalente de 200 unidades de fomento',
      initial_amount: 0,
      final_amount: 200,
      initial_days: 90,
      final_days: max_final_days,
      code: '33');

    Rank.create(
      description: 'Operaciones no reajustables en moneda nacional 90 días o más - Superiores al equivalente de 5.000 unidades de fomento',
      initial_amount: 5000,
      final_amount: max_final_ammount,
      initial_days: 90,
      final_days: max_final_days,
      code: '34');

    Rank.create(
      description: 'Operaciones no reajustables en moneda nacional 90 días o más - Inferiores o iguales al equivalente de 5.000 unidades de fomento y superiores al equivalente de 200 unidades de fomento',
      initial_amount: 200,
      final_amount: 5000,
      initial_days: 90,
      final_days: max_final_days,
      code: '35');
      
    Rank.create(
      description: 'Operaciones no reajustables en moneda nacional 90 días o más - Inferiores o iguales al equivalente de 200 unidades de fomento y superiores al equivalente de 50 unidades de fomento',
      initial_amount: 50,
      final_amount: 200,
      initial_days: 90,
      final_days: max_final_days,
      code: '44');

    Rank.create(
      description: 'Operaciones no reajustables en moneda nacional 90 días o más - Inferiores o iguales al equivalente de 50 unidades de fomento',
      initial_amount: 0,
      final_amount: 50,
      initial_days: 90,
      final_days: max_final_days,
      code: '45');

  end
  def down
    
    drop_table :ranks
  end
end
